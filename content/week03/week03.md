+++
title = "Week 3: Consultation"
lead = ""
+++

This week, I had sick of leaving for couples of days.

#### 01.02.2022 Consultation with Luke

<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col">
		We were discussing the paper, Force Jacket. It is a similar project to mine. What I can do?<br><br>
		<ul>
			<li>Identify the gaps: shows the differences</li>
			<li>Justify the relavant from the thesis</li>
		</ul>
		<div class="table-responsive">
		  <table class="table table-white table-striped">
		  <thead>
		    <tr>
		    	<th scope="col"></th>
		      	<th scope="col">Hugging Suit</th>
		      	<th scope="col">Force Jacket</th>      
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <th scope="row">Aims</th>
		      <td>Remote intimacy, Hug, Mobility, Pandemic</td>
		      <td>Haptic simulation in VR game</td>
		    </tr>
		    <tr>
		      <th scope="row">Technology</th>
		      <td>Mckibben Artificial Muscles, Air Reservoirs
		      </td>
		      <td>26 airbags, force sensitive sensors, vacuum pump and air compresssor</td>
		    </tr>
		    <tr>
		      <th scope="row">Literature Review</th>
		      <td>
		      	<ul>
		      		<li>Intimacy, Touch, Hug</li>
		      		<li>Haptic Simulation (On-body and Gloves)</li>
		      		<li>Pneumatic, Artificial Muscle</li>
		      	</ul>
		      </td>
		      <td>
		      	<ul>
		      		<li>On-Body Haptic Interfaces</li>
		      		<li>Pneumatic Interfaces</li>
		      		<li>Percetual User Studies with Haptics</li>
		      	</ul>
		      </td>
		    </tr>
		  </tbody>
		</table>
		</div>
		<ul>
			<li>My research questions should be specificly to the Remotely Hugging scenarios</li>
			<li>Describe the first prototype and user study in the my thesis</li>
			<li>Special idea can be written in the Discussion section</li>
		</ul>
	</div>
	<div class="col">
		<img class="img-fluid" src="../feedback1.jpg" alt="reset">
	</div>
</div>



#### 04.02.2022 Consultation with Eva and Sabine

<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col">
		Feedback:<br>
		<ul>
			<li>Think about how to use the Mckibben Artificial Muscle to create the feeling of hug directly? <span style="color: red">I don't know actually</span></li>
			<li>The work progress should be written in the thesis</li>
			<li>In the second user study, instead of questionnaires, I can ask some deeper questions by an short interview. For example:
				<ul>
					<li>What is the user scenario?</li>
					<li>Why, how, what?</li>
				</ul>
			</li>
			<li>The format for citation doesn't matter. I will go with  <span style="color: red">APA</span> that I learned before.</li>
			<li>How does Input go to Output?
				<ul>
					<li>Idealy, the 3x3 Mckibben grid will match with e-textile grid income</li>
				</ul>
				<img class="img-fluid" src="../feedback2.jpg" alt="reset">
			</li>
			<li>Literature Review: 1. Intimacy 2. Technology</li>
		</ul>
	</div>
	<div class="col">
		<img class="img-fluid" src="../feedback3.jpg" alt="reset">
	</div>
</div>


