+++
title = "Week 4: Literature Review"
lead = ""
+++

<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col pb-3">
		<h4>07.02.2022 Overleaf</h4>
		I went through the <a href="https://www.youtube.com/watch?v=y8y_KIs9JLs">tutorial</a> of Overleaf, in order to have a better arrangement of citations and paper layout. However, after two days of trying I found that it doesn't work so well with Zotero. Also, all the instinctive functions that I can easily click and use in Word, I need to google and find a script for it.<br> 
		I believe now the priority is to <b>develop the content and experiment of the master thesis</b>. So I decided to jump back to Word and work on it I used to be.
	</div>
	<div class="col">
		<img class="img-fluid" src="../overleaf.jpg" alt="reset">
	</div>
</div>

<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col pb-3">
		<h4>Translocal Mindfulness</h4>
		<b>What and aims:</b><br>
		<ol>
			<li>Encouraging mindful-meditation practice as a way to cultivate emotional awareness</li>
			<li>Becoming an intimate-human- interaction starter</li>
		</ol>
		<b>What I learned:</b><br>
		<ul>
			<li>Road map (Structure of the thesis) to the thesis can be present with <span style="color: red">an overview diagram</span>, as the Figure 2. from Force Jacket.</li>
			<li>The Arduino Code can be better presented with GitHub or such platform. It can refer to Wireless Haptic System</li>
		</ul>
		Resource: <a href="https://youtu.be/T80Dg-30e-0">Video</a>
	</div>
	<div class="col">
		<img class="img-fluid" src="../roadmap.jpg" alt="reset">
	</div>
</div>


#### Download
For taking the notes from the literature review more efficiently, I will upload the note file here directly.
- [Note for Week 4](../0211_Research_Note.docx)