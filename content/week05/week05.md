+++
title = "Week 5: Single Mckibben Artificial Muscle (Air Actuators)"
lead = ""
+++

<div class="row pb-3">
    <div class="col">
      <img class="img-fluid pb-3" src="../result1.jpg" alt="present">
    </div>
    <div class="col-4">
      <video class="img-fluid" controls width="800">
        <source src="../result2.mp4" type="video/mp4">
    </video>
    </div>
</div>


### 3D printed Mold (Cylinder)

<div class="row row-cols-1 row-cols-md-2">
    <div class="col">
      <img class="img-fluid pb-3" src="../cylinder1.jpg" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../cylinder2.jpg" alt="present">
    </div>
</div>

I had the inspiration from the [PneuFlex actuator](https://www.youtube.com/watch?v=Ss-9iXRUeGc) and the [documentation](https://www.robotics.tu-berlin.de/menue/software_tutorials/pneuflex_tutorial/) from the Robotics and Biology Laboratory of TU-Berlin. They made soft robotic fingers, so called PneuFlex actuator, and provide a well-documented website for most of the materials I need to rebuild the project. In this week, I will realize the **Single Mckibben Artificial Muscle** and find the ideal model of it, in terms of designed, shapes, and materials. Next two weeks, I will build it into **3x3 Mckibben Artificial Muscle Gird**.

<div class="row row-cols-1 row-cols-md-2">
  <div class="col pb-3">
    <iframe width="790" height="444" src="https://www.youtube.com/embed/Ss-9iXRUeGc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
  <div class="col pb-3">
    <img class="img-fluid" src="../cylinder4.jpg" alt="present">
  </div>
</div>

<div class="row row-cols-1 row-cols-md-2">
    <p>In the later of the documentations, Single Mckibben Artificical Muscle will be called as <b style="color: red">Air Actuators</b>. The Air Actuators will be the main components to provide the touch by expanding and shrinking the shape in the same direction, which should be able to realize with winded threads. The differences from PneuFlex actuator is that Air Actuator is going to expand in the <b>one direction</b>, instead of bending and simulating the movement of fingers.</p>
    <div class="col">
      <img class="img-fluid pb-3" src="../cylinder3.jpg" alt="present">
    </div>
</div>

<div class="row row-cols-1 row-cols-md-2">
    <div class="col">
      <img class="img-fluid pb-3" src="../mold1.jpg" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../mold2.jpg" alt="present">
    </div>
</div>

Following the concept of the PneuFlex, I made the mold in Fusion 360 and 3D printed it. Also, I made the **thread and zigzag** inside of the mold so that the threads have traces to follow and wind.

### Silicone Casting (Cylinder)

Since the air actuator is relative complicated shape, it has three molds and the casting procedure needs to be devided into three steps. **Chambers casting, winding, chambers closing.** 

<div class="row">
    <div class="col-5">
      <img class="img-fluid pb-3" src="../casting1.jpg" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../casting2.jpg" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../casting7.jpg" alt="present">
    </div>
</div>

In the Kunststoff workshop, it provides only [Sorta-Clear 40](https://www.kaupo.de/shop/SILIKONKAUTSCHUK-addition/SORTA-CLEAR-SERIE/SORTA-Clear-40/SORTA-Clear-40-2-Silikonkautschuk.html) sillicone, however it takes 16 hours to cure. In order to increase the efficiency, I ordered [Dragon Skin 10 Fast](https://www.kaupo.de/shop/SILIKONKAUTSCHUK-addition/DRAGON-SKIN-SERIE/Dragon-Skin-10-Fast/Dragon-Skin-10-Fast-1-Silikonkautschuk.html) that only needs 75 minutes to cure. Its texture is softer and more extendable, these characteristics are especially good for soft robotic projects.

#### 1. Chambers Casting

<div class="row">
    <div class="col">
      <img class="img-fluid pb-3" src="../casting3.jpg" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../casting4.jpg" alt="present">
    </div>
    <div class="col-3">
      <img class="img-fluid pb-3" src="../casting5.jpg" alt="present">
    </div>
</div>

#### 2. Winding

<div class="row row-cols-1 row-cols-md-2">
    <div class="col">
      <img class="img-fluid pb-3" src="../casting6.jpg" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../casting8.jpg" alt="present">
    </div>
</div>

#### 3. Chambers Closing

<div class="row row-cols-1 row-cols-md-2">
    <div class="col">
      <img class="img-fluid pb-3" src="../casting9.jpg" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../casting10.jpg" alt="present">
    </div>
</div>

### Results from Cylinder Air Actuators

<div class="row">
    <div class="col">
      <img class="img-fluid pb-3" src="../result3.jpg" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../cylinder2.jpg" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../result4.jpg" alt="present">
    </div>
</div>

<div class="row">
    <div class="col pb-3">
	    <b>No Trace</b>
	    <video class="img-fluid" controls width="800">
	       <source src="../result6.mp4" type="video/mp4">
	    </video>
    </div>
    <div class="col pb-3">
    	<b>Thread Trace</b>
	    <video class="img-fluid" controls width="800">
	       <source src="../result5.mp4" type="video/mp4">
	    </video>
    </div>
    <div class="col pb-3">
    	<b>Zigzag Trace</b>
	    <video class="img-fluid" controls width="800">
	       <source src="../result2.mp4" type="video/mp4">
	    </video>
    </div>
</div>

### Discussion and Improvements

From the videos, we can see the prototypes works as we expected. The Air Actuators expand and shrink obviously in the one direction for around <span style="color: blue">5-6 mm</span>. From the first prototypes I learned the following things:

1. What kind of trace for threads? <br>
From the first prototypes, I found that the traces make the winding procedure easier. There is not obvious different from Thread Trace and Zigzag Trace. It depends on the shape of air actuator. **Cylindrical air actuator is easier to be modeled with Thread Trace, and rectangular one is easier to be modeled with Zigzag Trace.**

2. Cylinder or Rectangle? <br>
Considering the later production of 3x3 Mckibben Grid, the Rectangular air actuator is more ideal shape because of thread winding and coverage. However, it will be answered after the second prototypes (rectangle) are done.

3. The walls of air chambers should be thin and soft enough to be extended.<br>
In the video can see that my hand is shaking, because I needed to squeeze so hard to inflate the air into chamber. Then I realized the wall of air chamber was <span style="color: blue">5.85 mm</span>. I looked back and observed the old design to see how the thickness of walls affect the changing of soft robot. The ideal wall thickness is <span style="color: red">3mm</span> and the silicone, Dragon Skin, is softer than Sorta-Clear.
	
	<div class="row">
	    <div class="col">
	      <img class="img-fluid pb-3" src="../improve1.jpg" alt="present">
	    </div>
	    <div class="col">
	      <img class="img-fluid pb-3" src="../improve2.jpg" alt="present">
	    </div>
	</div>
	<div class="row">
	    <div class="col">
	      <img class="img-fluid pb-3" src="../improve3.jpg" alt="present">
	    </div>
	    <div class="col">
	      <img class="img-fluid pb-3" src="../improve4.jpg" alt="present">
	    </div>
	    <div class="col">
	      <img class="img-fluid pb-3" src="../improve5.jpg" alt="present">
	    </div>
	</div>

4. The design of molds <br>
It was such a headache to design the molds by observing the referential molds, because it needs to image the change in between positive and negative. Later, I found it's easier to make the positive casting first and use the combine function to **cut** the mold and split it. This part will be described in the second prototypes. For the single Air Actuator, spliting the mold from <span style="color: blue">the side</span> is helpful for taking the mold off. For the 3x3 Air Actuator Gird, splitting from <span style="color: blue">the bottom</span> is the only option to keep the completeness of casting. 
	<div class="row">
	   <div class="col">
	      <img class="img-fluid pb-3" src="../improve6.jpg" alt="present">
	   </div>
	   <div class="col">
	      <img class="img-fluid pb-3" src="../improve7.jpg" alt="present">
	    </div>
	</div>

5. Air leak <br>
With the needles to inflate, air leak happened. Therefore, installing a 2mm outer tubing and sealing it with silicone as PneuFlex actuators did is necessary for later development. Or curing with installed tubings, but it depends on the design of molds.
	<div class="row">
	   <div class="col">
	      <img class="img-fluid pb-3" src="https://www.robotics.tu-berlin.de/fileadmin/fg170/Research/CIMG1640_Insert%20cannula.jpg" alt="present">
	    </div>
	</div>


### 3D printed Mold (Rectangle)

<div class="row row-cols-1 row-cols-md-2">
    <div class="col">
      <img class="img-fluid pb-3" src="../rectangle1.jpg" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../rectangle2.jpg" alt="present">
    </div>
</div>

With the points mentioned above, I made the second mold in rectangular shape that is opened from the side. To realize it, I went through the tutorials of [One-Part Mold](https://youtu.be/f-Zs3lx5OQA) and [Two-Part Mold](https://youtu.be/vKZx9eHEL6o). 

<div class="row row-cols-1 row-cols-md-3">
    <div class="col">
      <img class="img-fluid pb-3" src="../rectangle3.jpg" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../rectangle4.jpg" alt="present">
    </div>
    <div class="col">
      <img class="img-fluid pb-3" src="../rectangle5.jpg" alt="present">
    </div>
</div>

### References

- [Soft robotic toolkit - Pneumatic Artificial Muscles](https://softroboticstoolkit.com/book/pneumatic-artificial-muscles)

- [Soft robotic toolkit - Inner tubing](https://softroboticstoolkit.com/book/pam-tubing)

- [Soft robotic toolkit - Custom tubing](https://softroboticstoolkit.com/book/pam-custom-molding-method-1)

- [Documentation of PneuFlex actuator from Rotobtics and Biology Laboratory of TU-Berlin](https://www.robotics.tu-berlin.de/menue/software_tutorials/pneuflex_tutorial/)

- [Flat Pneumatic Actuators](https://youtu.be/LlxUZABcah0)

- [Tutorial for One-Part Mold on Fusion 360](https://youtu.be/f-Zs3lx5OQA)

- [Tutorial for Two-Part Mold on Fusion 360](https://youtu.be/vKZx9eHEL6o)

### Unread-related Papers

- [McKibben artificial muscles: pneumatic actuators with biomechanical intelligence](https://ieeexplore.ieee.org/document/803170)

- [Design and characterization of artificial muscles from wedge-like pneumatic soft modules](https://www.sciencedirect.com/science/article/abs/pii/S0924424719309884)

- [Valveless microliter combustion for densely packed arrays of powerful soft actuators](https://www.pnas.org/content/118/39/e2106553118.short?casa_token=YoDMPwb9gvUAAAAA:P5e28lXy_eaB19lh6gQQ0IoaV-XzuMTe1dPm6dWGPLcNc6nvnOiXueS10NpfU2edBTMS3z3-HjOcnA)

### Potiential Solutions for Air Controll

- [Pressure-Flow Control: It’s More Than Stabilizing Pressure](https://www.airbestpractices.com/system-assessments/pipingstorage/pressure-flow-control-it-more-stabilizing-pressure)

- [Types of Air Pressure Regulators: Precision vs. Filter vs. General Purpose Regulators](https://www.controlair.com/blog/types-of-air-pressure-regulators-precision-regulators-vs-filter-regulators-vs-general-purpose-regulators/)

- [Air valves from Digi-Key](https://www.digikey.fi/en/products/filter/pneumatics-hydraulics-valves-and-control/809?s=N4IgTCBcDaIIYEsBOACAbnANmgpiAugDQgCsUoADlGMRVZGCQL5NA)


### Download


The [Week 05 zip file](../week05_download.zip) includes:

- Mckibben Mold_Cylinder_Single.stl
- Mckibben Mold_Rectangle_Single.stl


