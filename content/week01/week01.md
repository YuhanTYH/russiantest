+++
title ="Week 1: Literature Review"
+++


### 11.01.2022 Research through Design (RtD)

<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col">
		<img class="img-fluid" src="../RtD.jpg" alt="RtD">	
	</div>
	<div class="col">
		<h4>What I learned:</h4>
		In this picture we would find ourselves as <span style="color: blue">interaction designers</span>. We reframe the technology according to the <span style="color: blue">needs, experience and usability</span> of the targets users, as well as their socio-cultural context. In the aspect of research, we create <span style="color: blue">a series of artefacts</span> to answer the <span style="color: blue">research questions</span>.<br><br>
		In the methodology section, firstly the paper gave an overview of the literature review, workshop, and interview. Later, each section gave a detailed discussion and conclusion from each aspect. All of these different perspective provides the powerful and concrete fundament for the module that the thesis proposed.<br><br>
		From this point of view, I can describe my first user study in the methodology section of my thesis. It can be seen as the first iteration of my thesis.
	</div>
</div>

Resource: [Zimmerman, J., Forlizzi, J., & Evenson, S. (2007). Research through design as a method for interaction design research in HCI.](https://dl.acm.org/doi/abs/10.1145/1240624.1240704)

### 18.01.2022 

#### Reset 

<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col">
		<b>What and aims:</b><br>
		A pod helps people to release the stress from daily life<br>
		<b>Why:</b><br>
		The project states that the stress is a global issue by presenting a picture with the costs and disadvantages behind it<br>
		<b>Target audiences: </b>Everyone in a work and study space<br>
		<b>How:</b><br>
		Input:<br>
		<ul>
			<li>Heart and brain sensors</li>
		</ul>
		Output:<br>
		<ul>
			<li>Interactive LED touchable façade with soft color</li>
			<li>A sofa to lay down</li>
			<li>Music to listen</li>
			<li>An app to visualize the physiological value</li>
		</ul>
		<b>Presentation: </b><br>Showing the users response by a video
	</div>
	<div class="col">
		<img class="img-fluid" src="../reset.jpg" alt="reset">
		Resource:<a href="https://www.unstudio.com/en/page/11771/reset-stress-reduction-pods">Unstudio</a> and <a href="http://getreset.eu">Reset</a> 
	</div>
</div>

<h4>ReSkin</h4>
<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col pb-3">
		<img class="img-fluid" src="../reskin1.jpg" alt="reset">
		<img class="img-fluid" src="../reskin2.jpg" alt="reset">
		<img class="img-fluid" src="../reskin3.jpg" alt="reset">
	</div>
	<div class="col">
		<b>What and aims:</b><br>
		A reusable and replaceable material can detected tactile information.<br>
		<b>How:</b><br>
		Magenetic sensor, Machine Learning<br>
		<b>Advantages:</b><br>
		Inexpensive (<$30), soft magnetized skin, high temporal and spatial resolution, replaceable<br>
		<b>Presentation: </b><br>
		Showing the differences and applications in the video<br>
		<b>What I learned:</b><br>
		<ul>
			<li>The design and fabrication pharagrahp can be putted after Background</li>
			<li>The structure of the paper can be applied in my thesis</li>
			<li>The project compared itself with other similar products</li>
			<li>Including the applications of ReSkin in both videos and words</li>
		</ul>
		Resource:<a href="https://reskin.dev">ReSkin</a>,  <a href="https://openreview.net/pdf?id=87_OJU4sw3V">Paper</a> and <a href="https://ai.facebook.com/blog/reskin-a-versatile-replaceable-low-cost-skin-for-ai-research-on-tactile-perception">Meta AI</a>
	</div>
</div>

### 19.01.2022 Equipment to build Prototypes

#### Digital Bauhaus Lab

[Digital Bauhaus Lab](https://www.uni-weimar.de/de/medien/institute/digital-bauhaus-lab/home/) has a soldering station, a sewing machine, and two 3D printers.

<div class="col pb-3">
	<img class="img-fluid" src="../dbl.jpg" alt="dbl">	
</div>

<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col pb-3">
		<h4>Kunststoff­ werkstatt</h4>
		<a href="https://www.uni-weimar.de/de/kunst-und-gestaltung/struktur/werkstaetten-ausstattung/zentrale-werkstaetten/kunststoff/">Kunststoff­ werkstatt</a> (a.k.a. Synthetic materials workshop) was suggested by <a href="https://www.uni-weimar.de/de/kunst-und-gestaltung/struktur/lehrgebiete-personen/produktdesign/timm-burkhardt/">Mr. Timm Burkhardt</a>, where has a vacuum table and <a href="https://www.kaupo.de/produkte/silikonkautschuk-additionsvernetzend/dragon-skin-serie/">Dragon Skin series silicone</a>. While I was looking around in the workshop, I saw one student was making a similar project. He used <a href="https://www.extremtextil.de/en/nylon-210den-tpu-coated-one-side-275g-sqm-heat-sealable.html">TPU-coated Nylon</a> to build a inflatable vest and a metal reservoir to compress the air which was made in the metal workshop.
	</div>
	<div class="col">
		<img class="img-fluid" src="../ksw.jpg" alt="ksw">	
	</div>
</div>

#### Modellwerkstätten
<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col pb-3">
		<img class="img-fluid" src="../mmw.jpg" alt="mmw">	
	</div>
	<div class="col">
		<a href="https://www.uni-weimar.de/en/architecture-and-urbanism/profile/workshops-and-labs/model-making-workshops/">Modellwerkstätten</a> (a.k.a Model Making Workshops) have a laser cutter (SPEEDY 500), a water jat cutter, and two 3D printers (ProJet 660 Pro for white and RAISE3D Pro2 Plus for different colors)
	</div>
</div>

















