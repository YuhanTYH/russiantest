+++
title = "Week 2: Literature Review"
lead = ""
+++

<h4>Force Jacket</h4>
<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col">
		<b>What and aims:</b><br>
		This paper aims to create an immersive experience in virtual reality by fulfilling the <b style="color: red">pressure receptors</b> on the <b style="color: red">full upper-body area</b>.<br>
		<b>How:</b><br>
		Force Jacket includes 26 airbags, force sensitive sensors, vacuum pump and air compresssor<br>
		<b>Advantages:</b><br>
		This paper clearly shows each part of the jacket, from overview, the design of the jacket, pneumatic control, to air actuation. From each perspective, readers can understand how and why the prototype was built in a specific way.<br>
		<b>What I learned:</b><br>
		<ul>
			<li>In the related works (literature review) paragraph, I can compare  similar projects in a table.</li>
			<li>Drawing down the overview of the project</li>
			<li>Show the airbags systems by a schematic of airbag tubing</li>
			<li style="color: red">Pressure and vibration are provided both by solenoid valves</li>
			<li>From the reference list, I found other papers to study</li>
		</ul>
		Resource: <a href="https://dl.acm.org/doi/abs/10.1145/3173574.3173894">Force Jacket: Pneumatically-Actuated Jacket for Embodied Haptic Experiences</a> and  <a href="https://youtu.be/YE1iDuOz-B0">Video</a>
	</div>
	<div class="row">
		<img class="img-fluid" src="../forcejacket1.jpg" alt="reset">
		<img class="img-fluid" src="../forcejacket2.jpg" alt="reset">
	</div>
</div>
<h4>Meta Reality Labs Research</h4>
<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="row row-cols-lg-1 row-cols-sm-2">
		<div class="col">
			<img class="img-fluid" src="../realitylab1.jpg" alt="reset">
		</div>
		<div class="col">
			<img class="img-fluid" src="../realitylab2.jpg" alt="reset">
		</div>
	</div>
	<div class="col">
		<b>What and aims:</b><br>
		Grasping an object and touching a surface in the virtual world<br>
		<b>How:</b><br>
		<ul>
			<li>Actuators: 15 ridged and <b style="color: blue">inflatable plastic pads</b></li>
			<li>White marker on the back for tracking</li>
			<li>Internal sensors for capturing the fingers' bending</li>
		</ul>
		<b>Advantages:</b><br>
		Replacing bulky motors with <b style="color: blue">tiny air valves</b><br>
		If it launches a haptic glove system, Meta can guarantee the system will work on the Qculus and encourage developers to use it.<br>
		<b>Disadvantages and expectations in future:</b><br>
		<ul>
			<li>Higher density of actuators</li>
			<li>Washable</li>
			<li>Can tell the different <b style="color: red">textures</b></li>
			<li>Lighter, because the tubings are bulky and sweltering</li>
			<li>Wireless</li>
			<li>Haptic gloves need to fit precisely against wearers’ skin</li>
		</ul>
		<b>What I learned:</b><br>
		<ul>
			<li>Combining VR with controlled vibrations can make people feel they’re touching something</li>
			<li><a href="https://www.theverge.com/2019/9/23/20881032/facebook-ctrl-labs-acquisition-neural-interface-armband-ar-vr-deal">CTRL-Labs (Electromyography team)</a> works on reading nerve signals on the arm and translates them into digital input</li>
		</ul>
		Resource: <a href="https://www.theverge.com/2021/11/16/22782860/meta-facebook-reality-labs-soft-robotics-haptic-glove-prototype">Meta Haptic Gloves</a>
	</div>
</div>
<h4>Unknown research from Meta Reality Labs</h4>
<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col pb-3">
		<img class="img-fluid" src="../realitylab3.jpg" alt="reset">
	</div>
	<div class="col">
		<b>What and aims:</b><br>
		Rather than employ bulky mechanical actuators, the team is developing tiny pneumatic and electroactive actuators for its VR/AR haptic glove<br>
		<b>How:</b><br>
		<b style="color: red">Don't know yet, can find the open resource</b><br>
		<b>Advantages:</b><br>
		<ul>
			<li>Smaller</li>
			<li>Lighter</li>
			<li>Electroactive Actuators</li>
		</ul>
		<b>What I learned:</b><br>
		It might be challenging for me to build such small size, but what I can do according to the discussion with Hannes. I can cast <b style="color: red">a silicone based station</b> that includes the way for <b style="color: blue">airflow and T-Valves</b>. Then it can keep the softness and simplicity from tubings.<br>
		<b>Resource: </b>
		<a href="https://newatlas.com/vr/facebook-meta-haptic-glove-virtual-reality/">Pneumatic and electroactive actuators</a>, <a href="https://youtu.be/MY9EdNo3gVA?t=281">Video</a>, and <a href="https://www.pnas.org/content/118/39/e2106553118.short?casa_token=YoDMPwb9gvUAAAAA:P5e28lXy_eaB19lh6gQQ0IoaV-XzuMTe1dPm6dWGPLcNc6nvnOiXueS10NpfU2edBTMS3z3-HjOcnA">similar paper</a>
	</div>
</div>
<h4>AxonVR's haptic box</h4>
<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col">
		<b>What and aims:</b><br>
		A possible tactile sensation<br>
		<b>How:</b><br>
		<ul>
			<li>HaptX platform uses tiny 200 actuators that put pressure on different points of the skin, changing temperature</li>
			<li>The temperature system works by pumping fluids around the array</li>
		</ul>
		<b>Advantages:</b><br>
		This method is far rarer than the usual vibration-based haptics<br>
		<b>Disadvantages:</b><br>
		<ul>
			<li>You <b style="color: red">can’t</b> use this hand for interactions</li>
			<li>The <b style="color: red">resolution</b> is still low</li>
			<li>It doesn't simulate <b style="color: red">textures</b></li>
		</ul>
		<b>Possible Application:</b><br>
		<ul>
			<li>Theme park attractions and other location-based entertainment</li>
		</ul>
		Resource: <a href="https://www.theverge.com/2017/3/2/14773870/axonvr-haptx-vr-haptic-box-pressure-temperature-gdc-2017">AxonVR’s haptic box</a>
	</div>
	<div class="row row-cols-lg-1 row-cols-sm-2">
		<div class="col">
			<img class="img-fluid" src="../hapticbox2.jpg" alt="reset">
		</div>
		<div class="col">
			<img class="img-fluid" src="../hapticbox1.jpg" alt="reset">
		</div>
	</div>
</div>

<h4>25.01.2022 Meeting with Hannes</h4>
<div class="row row-cols-1 row-cols-lg-2 pb-3">
	<div class="col">
		Journal List:<br>
		<ul>
			<li><a href="https://dl.acm.org">ACM (Association for Computing Machinery) Digital Library</a></li>
		</ul>
		Forum/Symposium:<br>
		<ul>
			<li><a href="https://sigchi.org/conferences/conference-history/uist/">ACM SigChi User Interface Software and Technology</a></li>
		</ul>
		Annual Conference:<br>
		<ul>
			<li><a href="https://tei.acm.org/2022/">ACM Tangible Embedded and Embodied Interaction</a></li>
		</ul>
		Pneumatic Componnents:<br>
		<ul>
			<li><a href="https://www.festo.com/cat/fi_fi/products_030000">Festo</a></li>
			Alternative Solutions for air reservoirs:
			<ul>
				<li>Metal Cylinder</li>
				<li>Cola Bottles</li>
				<li>Beer Compressed Cans</li>
				<div class="col-6">
					<img class="img-fluid" src="../solution3.jpg" alt="reset">
				</div>
			</ul>
		</ul>
		Silicone Stations:<br>
		<ul>
			<li><a href="https://www.digikey.fi/en/products/detail/adafruit-industries-llc/4663/13170960?cur=EUR&lang=en">Three-way solenoid valves</a>*6</li>
		</ul>
	</div>
	<div class="col">
		<img class="img-fluid" src="../solution1.jpg" alt="reset">
		<img class="img-fluid" src="../solution2.jpg" alt="reset">
	</div>
</div>




