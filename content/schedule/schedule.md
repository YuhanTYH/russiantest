+++
title ="Preparation 0: Schedule"
+++

This is the plan for accomplishing the master thesis in the following 16 weeks. It will be updated from time to time. Because the future is always unexpected. 🙌

## Master Thesis Schedule in 16 weeks (02. 02. 2022 updated)
<div class="table-responsive">
  <table class="table table-white table-striped">
  <thead>
    <tr>
      <th scope="col">Time</th>
      <th scope="col">To Do</th>
      <th scope="col">To Write</th>      
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Week 1 - 4<br>17.01 - 13.02.2022</th>
      <td>
        <ul>
          <li>Define what to build for prototype</li>
          <li>Prepare for the materials and equipment</li>
          <li style="color: blue;"><strike>Week 3 Consultation: 01. Feb. 11 pm. with Luke<br>04. Feb. 3-4 pm. with Sabine and Eva</strike></li>
        </ul>
      </td>
      <td>
        <ul>
          <li>Literature Review</li>
          <li><strike>Structure the thesis (by chapter)</strike></li>
          <li>Introduction</li>
        </ul>
      </td>
    </tr>
    <tr>
      <th scope="row">Week 5 - 8<br>14.02 - 13.03.2022</th>
      <td>
        <ul>
          <li>Prototype develops (6 + 2 weeks)</li>
          <li style="color: blue;">Week 8 Consultation: 08. Mar 11pm. with Luke</li>
          <li style="color: blue;">Week 9 Consultation: 14. Mar 2-3pm. with Sabine and Eva</li>
        </ul>
      </td>
      <td>
        <ul>
          <li>Methodology</li>
        </ul>
      </td>
    </tr>
    <tr>
      <th scope="row">Week 9 - 12<br>14.03 - 10.04.2022</th>
      <td>
        <ul>
          <li>Prototype develops (6 + 2 weeks)</li>
          <li>2nd User study (2 weeks)</li>
          <li style="color: blue;">Week 11 Consultation: 28.03-01.04</li>
        </ul>
      </td>
      <td>
        <ul>
          <li>Build the questionnaire</li>
        </ul>
      </td>
    </tr>
    <tr>
      <th scope="row">Week 13 - 16<br>11.04 - 08.05.2022</th>
      <td>
        <ul>
          <li style="color: blue;">Week 15 Consultation: 25.04-29.04</li>
        </ul>
      </td>
      <td>
      	<ul>
          <li>Results</li>
          <li>Discussion</li>
          <li>Conclusion</li>
          <li>Abstract</li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>
</div>




- Prototype develops (6 + 2 weeks)
  + Single McKibben artificial muscle (3D printed mold + Silicon casting)
  + 3x3 artificial muscle grids
  + Buy or build an air-compressed reservoir (Depends on budget) 
  + PCB (design + manufacture + soldering + test)
  + Sewing the hidden pocket

- Presentation
  + Video (1 Week presenting the function of prototype, filming on 2n user study)
  + Booklet (Traditional/1 Week/Overleaf)
  + Prototype itself

- 2nd User study (2 week)
	+ Build the questionnaire (2 week)

- ~~Structure the thesis (by chapter) (1 weeks)~~

- Literature Review (3 weeks) 
	+ Related works review
	+ Literature Review
	+ Movie or Series inspiration

- Introduction
	+ ~~Define the thesis topic~~
	+ Define the research 3+ questions or hypothesis
	+ What have I done?
	+ Did it work?
	+ Summery of the feedback?
	+ What I am going to do in this thesis?

- Methodology
	+ Research through Design (RtD)
	+ Answer the research 3+ questions
	+ User study (Design Questionnaire)


- Consultation
	+ Dr. Luke Hespanhol and Dr. Sabine Zierold once a month
	+ Prof. Eva Hornecker every two weeks











